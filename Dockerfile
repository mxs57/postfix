FROM registry.access.redhat.com/ubi8/ubi-init

LABEL io.k8s.description="This Postfix image is customised to suits needs." \
    io.k8s.display-name="Postfix image for openshift" \
	io.openshift.tags="postfix,mta,mailrelay" \
	name="postfix"

USER root

RUN yum -y install postfix rsyslog mailx \
	&& yum clean all \
	&& rm -rf /var/lib/yum/* \
	&& systemctl enable postfix \
	&& systemctl enable rsyslog

EXPOSE 10025

COPY etc/postfix/* /etc/postfix/
#COPY etc/rsyslog.d/maillog-to-stdout.conf /etc/rsyslog.d/

CMD ["/sbin/init"]
